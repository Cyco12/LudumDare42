﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    // Crosshair follower variables
    [System.Serializable]
    public class CrosshairFollow
    {
        public float speed = 5f;
        public Vector2 direction;
        public float angle;
        public Quaternion rotation;
    }
    public CrosshairFollow crosshairfollow;

    public GameObject gunObject;
    public GameObject playerObject;
    public GameObject shot;
    public Transform shotSpawn;
    public Vector3 shotSpawnVector; // The vector3 storing the vector values of the shotSpawn
    public float fireRate = 0.5f; // The rate of fire for abilities
    public float nextFire = 0.0f; // Variable for the time between fires; Cooldown is added to this

    public int magCapacity = 4;
    public int mag = 4;
                
    public SpriteRenderer SpriteRenderer; // Variable to hold a reference to our SpriteRenderer component
    public Animator weaponAttackAnimator;
    public Animator animatorShotSpawn;
    public float damage = 2.0f; // Amount of damage skill does to enemies

    public float shotSpeed = 5f;
    public Vector3 shotDirection;
    public Vector2 mousePosition;
    public Vector3 shotSpawnPosition;
    public Vector2 shotSpawnPositionVector2;
    public float shotAngle;
    public Quaternion shotRotation;

    // Raycasting
    public RaycastHit hit;
    public Ray ray;
    public float maxRaycastDistance = 100;
    public LayerMask whatToHit;
    public GameObject enemyObject;


    // Raycasting/Collision based damage
    public bool isHitscan;


    public KeyCode shoot;



    void Effect()
    {
        Instantiate(shot, shotSpawn.position, shotRotation);
    }

    void bulkGunShoot()
    {
        mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        shotSpawnPosition = new Vector3(shotSpawn.position.x, shotSpawn.position.y, -1.1f);
        shotSpawnPositionVector2 = new Vector2(shotSpawn.position.x, shotSpawn.position.y);
        shotDirection = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - shotSpawn.position.x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y - shotSpawn.position.y, -1.1f);//shotSpawn.position.z);
        ray = new Ray(shotSpawnPosition, shotDirection);//hit = Physics.Raycast(shotSpawnPosition, shotDirection, maxRaycastDistance, whatToHit);

        shotAngle = Mathf.Atan2(shotDirection.y, shotDirection.x) * Mathf.Rad2Deg;
        shotRotation = Quaternion.AngleAxis(shotAngle, Vector3.forward);
        shotSpawn.rotation = Quaternion.Slerp(shotSpawn.rotation, shotRotation, shotSpeed * Time.deltaTime);

        if (isHitscan == false)
        {
            Effect();
        }
        Vector3 rayDirection = new Vector3(mousePosition.x - shotSpawnPositionVector2.x, mousePosition.y - shotSpawnPositionVector2.y, 0);
        Debug.DrawLine(shotSpawnPosition, rayDirection * 100, Color.cyan);

        if (Physics.Raycast(ray, out hit, whatToHit))
        {
            enemyObject.GetComponent<MobController>().stats.health -= damage;

            Debug.DrawLine(hit.point, hit.point + rayDirection, Color.red);
            Debug.Log("We hit " + hit.collider.name + " and did " + damage + " damage.");
        }
        weaponAttackAnimator.SetBool("gunShooting", false);
    }

    void GunShoot()
    {
        bulkGunShoot();
        if (mag > 0)
        {
            Debug.LogWarning("mag > 0: " + mag);
            if (playerObject.GetComponent<PlayerController>().equippedWeapon == "USBStickShotgun")
            {
                if (mag == 4)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 1);
                    // bulkGunShoot();
                }
                if (mag == 3)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 2);
                    // bulkGunShoot();
                }
                if (mag == 2)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 3);
                    // bulkGunShoot();
                }
                if (mag == 1)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 4);
                    // bulkGunShoot();
                }
            }

            if (playerObject.GetComponent<PlayerController>().equippedWeapon == "WirelessUSBPistol")
            {
                if (mag == 10)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 1);
                    // bulkGunShoot();
                }
                if (mag == 9)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 2);
                    // bulkGunShoot();
                }
                if (mag == 8)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 3);
                    // bulkGunShoot();
                }
                if (mag == 7)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 4);
                    // bulkGunShoot();
                }
                if (mag == 6)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 5);
                    // bulkGunShoot();
                }
                if (mag == 5)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 6);
                    // bulkGunShoot();
                }
                if (mag == 4)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 7);
                    // bulkGunShoot();
                }
                if (mag == 3)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 8);
                    // bulkGunShoot();
                }
                if (mag == 2)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 9);
                    // bulkGunShoot();
                }
                if (mag == 1)
                {
                    weaponAttackAnimator.SetBool("gunShooting", true);
                    weaponAttackAnimator.SetInteger("fireStage", 10);
                    // bulkGunShoot();
                }
            }

            /*if (playerObject.GetComponent<PlayerController>().equippedWeapon == "AudioDongle")
            {
                weaponAttackAnimator.SetBool("gunShooting", true);
            }*/
            //weaponAttackAnimator.SetBool("gunShooting", false);
            mag -= 1;

            if (mag <= 0)
            {
                Debug.LogWarning("mag <= 0: " + mag);
                mag = magCapacity;
            }
        }  
    }


    // Use this for initialization
    void Start ()
    {
        // TODO: implement something similar to  `playermovekeys.right = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right", "RightArrow"));` to use keybindings
        shoot = KeyCode.Mouse0;

        enemyObject = GameObject.FindWithTag("Enemy");
        playerObject = GameObject.FindWithTag("Player");

        if (shotSpawn == null)
        {
            Debug.Log("playergun.shotSpawn is not assigned");
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Check for semi-auto
        if (fireRate == 0)
        {
            if (Input.GetKeyDown(shoot))
            {
                GunShoot();
                //GetComponent<AudioSource>().Play();
            };
        }
        // Check for full-auto
        else
        {
            if (Input.GetKey(shoot) && Time.time > nextFire)
            {
                GunShoot();
                nextFire = Time.time + 1 / fireRate;
                //GetComponent<AudioSource>().Play();
            };
        }

        // Crosshair follow
        crosshairfollow.direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        crosshairfollow.angle = Mathf.Atan2(crosshairfollow.direction.y, crosshairfollow.direction.x) * Mathf.Rad2Deg;
        crosshairfollow.rotation = Quaternion.AngleAxis(crosshairfollow.angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, crosshairfollow.rotation, crosshairfollow.speed * Time.deltaTime);
    }
}
