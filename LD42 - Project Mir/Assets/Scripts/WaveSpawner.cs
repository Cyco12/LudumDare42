﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public enum SpawnState {SPAWNING, WAITING, COUNTING};

    [System.Serializable]
    public class Wave
    {
        public string name;
        public GameObject enemy;
        public int count;
        public float rate;
    }

    public Wave[] waves;
    private int nextWave = 0;

    public Transform[] spawnPoints;
    public GameObject waveSpawnObject;

    public float timeBetweenWaves = 5f;
    public float waveCountdown;

    private float searchCountdown = 1f;

    public SpawnState state = SpawnState.COUNTING;


    void Start()
    {
        Debug.Log("[WaveSpawner.cs Start() beginning] Z position is currently " + transform.position.z);
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced");
        }

        waveCountdown = timeBetweenWaves;
        Debug.Log("[WaveSpawner.cs SpawnEnemy() ending] Z position is currently " + transform.position.z);
    }

    void Update()
    {
        if (state == SpawnState.WAITING)
        {
            if (!EnemyIsAlive())
            {
                WaveCompleted();
                Debug.Log("Wave Completed");
                return;
            }
            else
            {
                return;
            }
        }

        if (waveCountdown <= 0f)
        {
            if (state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountdown = waveCountdown - Time.deltaTime;
        }
    }

    void WaveCompleted()
    {
        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length)
        {
            nextWave = 0;
            Debug.Log("Completed All Waves");
            return;
        }
        else
        {
            nextWave++;
        }
    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;

        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        Debug.Log("Spawning Wave: " + _wave.name);
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        state = SpawnState.WAITING;

        yield break;
    }


    void SpawnEnemy(GameObject _enemy)
    {
        Debug.Log("[WaveSpawner.cs SpawnEnemy() beginning] Z position is currently " + transform.position.z);
        // Spawn enemy
        Debug.Log("Spawning Enemy: " + _enemy.name);

        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];

        Instantiate(_enemy, _sp.position, _sp.rotation);

        //_sp.position = new Vector3(_sp.position.x, _sp.position.y, -1.1f);
        Transform cachedTransform;
        cachedTransform = _sp.transform;
        cachedTransform.position = new Vector3(_sp.transform.position.x, _sp.transform.position.y, Mathf.Clamp(_sp.transform.position.z, -1.1f, -1.1f));

        _enemy.transform.parent = null;
        _enemy.transform.position.Set(cachedTransform.position.x, cachedTransform.position.y, cachedTransform.position.z);
        //_enemy.transform.position = cachedTransform.position;
        Debug.Log("[WaveSpawner.cs SpawnEnemy() ending] Z position is currently " + transform.position.z);
    }
}
