﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileSpriteSelector : MonoBehaviour
{
    //public Sprite spT, spB, spR, spL, spTB, spTB2, spRL, spTR, spTL, spBR, spBL, spTLB, spRTL, spBRT, spLBR, spTBRL;
    public GameObject tileDefault, tileT, tileB, tileR, tileL, tileTB, tileTB2, tileRL, tileTR, tileTL, tileBR, tileBL, tileTLB, tileRTL, tileBRT, tileLBR, tileTBRL;
    public bool top, bottom, right, left;
    public int type; // 0: normal, 1: enter
    public Color normalColor, enterColor;
    Color mainColor;
    SpriteRenderer rend;


    public GameObject PickTile()
    {
        if (top)
        {
            if (bottom)
            {
                if (right)
                {
                    if (left)
                    {
                        return tileTBRL;
                    }
                    else
                    {
                        return tileBRT;
                    }
                }
                else if (left)
                {
                    return tileTLB;
                }
                else
                {
                    return tileTB;
                }
            }
            else
            {
                if (right)
                {
                    if (left)
                    {
                        return tileRTL;
                    }
                    else
                    {
                        return tileTR;
                    }
                }
                else if (left)
                {
                    return tileTL;
                }
                else
                {
                    return tileT;
                }
            }
        }
        if (bottom)
        {
            if (right)
            {
                if (left)
                {
                    return tileLBR;
                }
                else
                {
                    return tileBR;
                }
            }
            else if (left)
            {
                return tileBL;
            }
            else
            {
                return tileB;
            }
        }
        if (right)
        {
            if (left)
            {
                return tileRL;
            }
            else
            {
                return tileR;
            }
        }
        return null;
    }

    void PickColor()
    {
        if (type == 0)
        {
            mainColor = normalColor;
        }
        else if (type == 1)
        {
            mainColor = enterColor;
        }
        rend.color = mainColor;
    }

    void Start()
    {
        rend = GetComponentInChildren<SpriteRenderer>();
        mainColor = normalColor;
        PickTile();
    }
}
