﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour {

    public Transform playertransform;

    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

	// Use this for initialization
	void Start () {
        if (playertransform == null) // Checks for PlayerTransform Transform variable to see if its null
        {
            playertransform = GameObject.FindGameObjectWithTag("Player").transform; // Assigns PlayerTransform Transform variable to the object.transform for the GameObject with the tag Player
        }

        _cameraOffset = transform.position - playertransform.position;
	}
	
	// LateUpdate is called after Update
	void LateUpdate () {
        Vector3 newPos = playertransform.position + _cameraOffset;

        transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);
	}
}
