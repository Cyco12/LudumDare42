﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayOnTop : MonoBehaviour
{
    // Crosshair follower variables
    [System.Serializable]
    public class Follow
    {
        public float speed = 5f;
        public Vector2 direction;
        public float angle;
        public Quaternion rotation;
        public bool active;
    }

    public Transform ontoptransform;
    public SpriteRenderer ontoprenderer;
    public Transform offsettransform;
    public Rigidbody ontopParentRigidbody;
    public Transform followtransform;
    private Vector3 _cameraOffset;
    public Follow follow; // Follower class

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    // Use this for initialization
    void Start()
    {
        if (followtransform == null)
        {
            followtransform = GameObject.FindGameObjectWithTag("Player").transform;
        }

        _cameraOffset = ontoptransform.position - offsettransform.position;
    }

    // LateUpdate is called after Update
    void LateUpdate()
    {
        Vector3 newPos = new Vector3(ontoptransform.position.x, ontoptransform.position.y, Mathf.Clamp(ontoptransform.position.z, -1.1f, -1.1f)) + _cameraOffset;
        Mathf.Clamp(ontoptransform.rotation.x, 0, 0);
        Mathf.Clamp(ontoptransform.rotation.y, 0, 0);
        Mathf.Clamp(ontoptransform.rotation.z, 0, 0);

        ontoptransform.position = new Vector3(offsettransform.position.x, offsettransform.position.y, Mathf.Clamp(ontoptransform.position.z, -1.2f, -1.2f));

        //Debug.Log("Velocity is " + ontopParentRigidbody.velocity.x);
        if (ontopParentRigidbody.velocity.x > 0)
        {
            //Flip the sprites
            ontoprenderer.flipX = true;
        }
        //Rotate/transform/flip objects if moving left
        if (ontopParentRigidbody.velocity.x < 0)
        {
            //Flip the sprites
            ontoprenderer.flipX = false;
        }

        // TODO: Fix code below so StayOnTop object has the option to always point towards a follower target
        /*follow.direction = Camera.main.ScreenToWorldPoint(followtransform.position) - ontoptransform.position;
        follow.angle = Mathf.Atan2(follow.direction.y, follow.direction.x) * Mathf.Rad2Deg;
        follow.rotation = Quaternion.AngleAxis(follow.angle, Vector3.forward);
        ontoptransform.rotation = Quaternion.Slerp(ontoptransform.rotation, follow.rotation, follow.speed * Time.deltaTime);*/
    }
}
