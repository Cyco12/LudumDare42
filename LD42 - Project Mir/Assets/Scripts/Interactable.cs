﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    public TextMeshProUGUI titleText, descText, flavText, damageNumText, firerateNumText, magSizeNumText, reloadTimeNumText;
    public GameObject uiMenu;
    public float itemDamage, itemFirerate, itemMagSize, itemReloadTime;
    public string itemName, itemDesc, itemFlavor;
    public Slider damageSlider, firerateSlider, magSizeSlider, reloadTimeSlider;
    public string scenename;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
