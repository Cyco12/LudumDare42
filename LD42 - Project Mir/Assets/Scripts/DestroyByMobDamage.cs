﻿//Libraries/APis
using UnityEngine;
using System.Collections;

public class DestroyByMobDamage : MonoBehaviour
{
    //Game boundary class
    [System.Serializable]
    public class Boundary
    {
        public float xMin, xMax, yMin, yMax;
    }

    //Variables
    public Rigidbody rb;
    public GameObject vfx1;
    public GameObject vfx2;
    public GameObject enemyObject;
    public Boundary boundary;
    public PlayerController playercontroller;
    public MobController mobcontroller;
    public string abilityName;


    IEnumerator VFXDelay(float time)
    {
        //Instantiate VFX GameObject assigned to vfx1 variable on the position of the object (other) that the gameObject is colliding with
        Instantiate(vfx1, transform.position, transform.rotation);
        yield return new WaitForSeconds(time);
        Destroy(GameObject.FindWithTag("ParticleEffect"));

        yield return null;
    }



    //When something enters collider trigger area
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Boundary")
        {
            Debug.Log("[other.gameObject]Object in Collision with this one is " + other.gameObject);
            Debug.Log("[other.gameObject.tag]Tag of object in Collision with this one is " + other.gameObject.tag);
        }

        if (other.gameObject.tag == "Boundary")
        {
            return;
        }

        if (other.gameObject.tag == "Player")
        {
            // Finds the gameObject of the 'other', the object that the object attached to this script is colliding with. In this case it is the object with an player tag.
            enemyObject = other.gameObject;

            // Checks for PlayerController component stored in mobcontroller. If null, assign value to it by finding the MobController component in the enemy's GameObject (stored in enemyObject).
            if ((playercontroller == null) && (enemyObject.GetComponent<PlayerController>().playerstats != null))
            {
                // Find MobController type component
                PlayerController playercontroller = enemyObject.GetComponent<PlayerController>();
                // Log mobcontroller stored value
                Debug.Log("[playercontroller] is " + playercontroller);
                // Log turtle health
                Debug.Log("[playercontroller.playerstats.health]Player Health: " + playercontroller.playerstats.health);

                if ((playercontroller.playerstats.health != 0) && (playercontroller != null))
                {
                    Debug.Log("[abilityName]Ability is " + abilityName);
                    // Subtracts amount of damage done by mob's ability from the players's health
                    playercontroller.playerstats.health -= gameObject.GetComponent<MobController>().abilities.bite.damage;
                    Debug.Log("[mobcontroller.stats.health]Post-Attack Enemy Health: " + mobcontroller.stats.health);
                }
            }
            // If there is no PlayerController component assigned to this GameObject, output log text for debugging
            else
            {
                Debug.LogWarning("Missing PlayerController component. Please add one");
            }

            //StartCoroutine(VFXDelay(5));
        }

        if (other.gameObject.tag == "Enemy")
        {
            return;
        }
    }
}