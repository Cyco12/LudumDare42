﻿//Libraries/APIs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using TMPro;


//Player Controller class and code
public class PlayerController : MonoBehaviour
{
    // Variables


    // Game boundary class
    [System.Serializable]
    public class Boundary
    {
        public float xMin, xMax, yMin, yMax;
    }

    // Player rotation class
    [System.Serializable]
    public class PlayerRotation
    {
        public float xRotation, yRotation, zRotation;
    }

    // Player stats class
    [System.Serializable]
    public class PlayerStats
    {
        public float health, experience, healthRegenRate;
    }

    // Player movement keybindings class
    [System.Serializable]
    public class PlayerMoveKeys
    {
        public KeyCode left, right, up, down, shoot, escape;
    }

    // Crosshair follower variables
    [System.Serializable]
    public class CrosshairFollow
    {
        public float speed = 5f;
        public Vector2 direction;
        public float angle;
        public Quaternion rotation;
        public bool active;
    }


    public Boundary boundary; // Player Boundary
    public PlayerRotation playerrotation; // Player Rotation
    public PlayerStats playerstats; // Player Stats
    public PlayerMoveKeys playermovekeys; // Player keycodes
    public CrosshairFollow crosshairfollow; // Crosshair Follower class
    public Vector3 weaponVector3;
    public KeyCode interact;
    public string equippedWeapon;
    public GameObject weaponObject;
    public GameObject usbstickshotgun;
    public GameObject audiodongle;
    public GameObject wirelessusbpistol;
    public bool weaponActive;
    public Weapon weapon;
    public Animator playerAnimator;
    public GameObject escapeMenu;
    public GameObject gameOverMenu;

    public Texture2D crosshairTexture;
    public CursorMode curMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;


    // Player Physics/Movement
    public Rigidbody rb;
    public float speed;
    public float tilt;

    // Variables to hold a reference to our SpriteRenderer components
    public SpriteRenderer characterSpriteRenderer;
    public SpriteRenderer weaponSpriteRenderer;

    // Transforms
    public Transform characterTransform;
    public Transform weaponTransform;
    public Transform weaponSpawn;


    // Coroutines


    // Check player stats at the end of each frame and update them accordingly
    IEnumerator checkStats()
    {
        // Check if player is 'dead'
        if (playerstats.health <= 0)
        {
            Destroy(gameObject);
            gameOverMenu.SetActive(true);
        }
        // Mana Regen
        if (playerstats.health < 20)
        {
            playerstats.health = playerstats.health + playerstats.healthRegenRate;
        }
        yield return new WaitForEndOfFrame();
    }

    //void Start runs code at the startup
    void Start()
    {
        interact = KeyCode.E;
        playermovekeys.left = KeyCode.A;
        playermovekeys.right = KeyCode.D;
        playermovekeys.escape = KeyCode.Escape;

        if (PlayerPrefs.GetString("equippedWeapon") != null)
        {
            equippedWeapon = PlayerPrefs.GetString("equippedWeapon");
        }


        //playermovekeys.left = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left", "LeftArrow"));
        //playermovekeys.right = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right", "RightArrow"));

        rb = GetComponent<Rigidbody>();
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Interactable")
        {
            if (Input.GetKeyDown(interact))
            { 

                Interactable interactable = other.GetComponent<Interactable>();

                interactable.titleText.text = interactable.itemName;
                interactable.descText.text = interactable.itemDesc;
                interactable.flavText.text = interactable.itemFlavor;
                interactable.damageNumText.text = interactable.itemDamage.ToString();
                interactable.firerateNumText.text = interactable.itemFirerate.ToString();
                interactable.magSizeNumText.text = interactable.itemMagSize.ToString();
                interactable.reloadTimeNumText.text = interactable.itemReloadTime.ToString();
                interactable.damageSlider.value = interactable.itemDamage;
                interactable.firerateSlider.value = interactable.itemFirerate;
                interactable.magSizeSlider.value = interactable.itemMagSize;
                interactable.reloadTimeSlider.value = interactable.itemReloadTime;


                if (interactable.uiMenu.activeSelf == true)
                {
                    interactable.uiMenu.SetActive(false);
                }
                if (interactable.uiMenu.activeSelf == false)
                {
                    interactable.uiMenu.SetActive(true);
                }
            }
        }

        if (other.tag == "Portal")
        {
            if (Input.GetKeyDown(interact))
            {
                Interactable interactable = other.GetComponent<Interactable>();
                SceneManager.LoadScene(interactable.scenename);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Room")
        {
            Debug.Log("Entered Room Collider");
            RoomType room = other.GetComponent<RoomType>();
            room.waveSpawnerObject.SetActive(true);
        }

        if (other.tag == "Enemy")
        {
            playerstats.health -= other.GetComponent<MobController>().abilities.bite.damage;
            return;
        }
    }


    public void SetEquippedWeapon(string setWeapon)
    {
        Debug.Log("Equipped Weapon Set to " + setWeapon + "!");
        PlayerPrefs.SetString("equippedWeapon", setWeapon);
        //PlayerPrefs.Save();
    }


    //void Update runs code every frame
    void Update()
    {
        StartCoroutine(checkStats());
        if (weaponActive == false)
        {
            if (PlayerPrefs.GetString("equippedWeapon") != null)
            {
                equippedWeapon = PlayerPrefs.GetString("equippedWeapon");
            }
        }
        if (equippedWeapon == "USBStickShotgun" && weaponActive == false)
        {
            weaponObject = Instantiate(usbstickshotgun, weaponSpawn);
            weaponTransform = usbstickshotgun.transform;
            weaponSpriteRenderer = usbstickshotgun.GetComponent<SpriteRenderer>();
            weapon = usbstickshotgun.GetComponent<Weapon>();
            weaponActive = true;
        }

        if (equippedWeapon == "AudioDongle" && weaponActive == false)
        {
            weaponObject = Instantiate(audiodongle, weaponSpawn);
            weaponTransform = audiodongle.transform;
            weaponSpriteRenderer = audiodongle.GetComponent<SpriteRenderer>();
            weapon = audiodongle.GetComponent<Weapon>();
            weaponActive = true;
        }
        if (equippedWeapon == "WirelessUSBPistol" && weaponActive == false)
        {
            weaponObject = Instantiate(wirelessusbpistol, weaponSpawn);
            weaponTransform = wirelessusbpistol.transform;
            weaponSpriteRenderer = wirelessusbpistol.GetComponent<SpriteRenderer>();
            weapon = wirelessusbpistol.GetComponent<Weapon>();
            weaponActive = true;
        }
        if (weaponActive == true && PlayerPrefs.GetString("equippedWeapon") != equippedWeapon)
        {
            Destroy(weaponObject);
            weaponActive = false;
        }

        characterTransform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        
        //Rotate/transform/flip objects if moving right
        if (Input.GetKey(playermovekeys.right))
        {
            playerAnimator.SetBool("isWalking", true);
            //Flip the sprites
            characterSpriteRenderer.flipX = false;
            //weaponSpriteRenderer.flipY = true;
            //Transform weapon location
            //weaponTransform.position = new Vector3(rb.position.x + 0.8f, rb.position.y + 0.2f, 0.0f);
            //Transform weapon rotation
            //weaponTransform.rotation = Quaternion.Euler(0.0f, 0.0f, -11.46f);
        }
        //Rotate/transform/flip objects if moving left
        if (Input.GetKey(playermovekeys.left))
        {
            playerAnimator.SetBool("isWalking", true);
            //Flip the sprites
            characterSpriteRenderer.flipX = true;
            //weaponSpriteRenderer.flipY = false;
            //Transform weapon location
            //weaponTransform.position = new Vector3(rb.position.x + -0.8f, rb.position.y + 0.2f, 0.0f);
            //Transform weapon rotation
            //weaponTransform.rotation = Quaternion.Euler(0.0f, 0.0f, 11.46f);
        }
        if (!Input.GetKey(playermovekeys.left) && !Input.GetKey(playermovekeys.right))
        {
            playerAnimator.SetBool("isWalking", false);
        }

        if (weapon.crosshairfollow.angle > -90.0 && weapon.crosshairfollow.angle < 90.0)
        {
            //Debug.Log("flipY should be false here");
            weaponSpriteRenderer.flipY = false;
        }

        if ((weapon.crosshairfollow.angle > 90.0 && weapon.crosshairfollow.angle < 180.0) ||
            (weapon.crosshairfollow.angle > -180.0 && weapon.crosshairfollow.angle < -90.0))
        {
            //Debug.Log("flipY should be true here");
            weaponSpriteRenderer.flipY = true;
        }

        if (Input.GetKey(playermovekeys.escape))
        {
            Cursor.SetCursor(null, hotSpot, curMode);
            escapeMenu.SetActive(true);
        }
        /*if (Input.GetKeyDown(playermovekeys.escape) || (escapeMenu.activeSelf == true))
        {
            escapeMenu.SetActive(false);
        }*/


        // Crosshair follow
        if (crosshairfollow.active == true)
        {
            crosshairfollow.direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            crosshairfollow.angle = Mathf.Atan2(crosshairfollow.direction.y, crosshairfollow.direction.x) * Mathf.Rad2Deg;
            crosshairfollow.rotation = Quaternion.AngleAxis(crosshairfollow.angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, crosshairfollow.rotation, crosshairfollow.speed * Time.deltaTime);
        }
    }

    //void FixedUpdate runs code (at the end of?) every frame
    void FixedUpdate()
    {
        /*if (Input.GetKey(playermovekeys.left) || Input.GetKey(playermovekeys.right))
        {*/
            //Input for movement from Joystick
            float moveHorizontal = Input.GetAxis("Horizontal"); //For Mobile    float moveHorizontal = CrossPlatformInputManager.GetAxis("Horizontal");     
            float moveVertical = Input.GetAxis("Vertical"); //For Mobile    float moveVertical = CrossPlatformInputManager.GetAxis("Vertical");

            //Movement variable measures movement on the scene
            Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);
            //Uses movement and global speed variable to determine how fast and where the player moves
            rb.velocity = (movement * speed);
            rb.position = new Vector3
            (
                Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
                Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax),
                -1.1f
            );
        //}
    }

    // This function is called just one time by Unity the moment the game loads
    private void Awake()
    {
        // get a reference to the SpriteRenderer component on this gameObject
        characterSpriteRenderer = GetComponent<SpriteRenderer>();
    }
}