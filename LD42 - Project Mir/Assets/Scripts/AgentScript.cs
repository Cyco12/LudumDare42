﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Pathfinding;

public class AgentScript : MonoBehaviour
{
    public GameObject target;
    private NavMeshAgent agent;
    public float lookRadius = 10f;


    // Use this for initialization
    void Start ()
    {
        agent = GetComponent<NavMeshAgent>();	
	}
	
	// Update is called once per frame
	void Update ()
    {
        Ray ray = Camera.main.ScreenPointToRay(target.transform.position);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            float distance = Vector3.Distance(target.transform.position, transform.position);
            if (distance <= lookRadius)
            {
                agent.SetDestination(target.transform.position);
            }
        }
	}
}
