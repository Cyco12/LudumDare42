﻿// Libraries/APIs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Pathfinding;

[RequireComponent (typeof (Rigidbody))]
[RequireComponent(typeof(Seeker))]
// Mob Controller class and code
public class MobController : MonoBehaviour
{
    // Mob Physics/Movement
    [System.Serializable]
    public class Physics
    {
        public Rigidbody rb;
        public float speed;
        public float tilt;
    }
    public Physics physics;

    // Mob boundary class
    [System.Serializable]
    public class Boundaries
    {
        public float xMin;
        public float xMax;
        public float yMin;
        public float yMax;

        public float rangeMin;
        public float rangeMax;
    }
    public Boundaries boundaries;

    // Mob Stats
    [System.Serializable]
    public class Stats
    {
        public float health = 10;
        public float level;
    }
    public Stats stats;

    // Mob Counter Variables for Mob Spawning
    [System.Serializable]
    public class CounterVariables
    {
        public int mobMode = 1;
        public int levelMobCount;
    }
    public CounterVariables countervars;

    // Miscellaneous Mob Variables
    public SpriteRenderer characterSpriteRenderer; // variable to hold a reference to our SpriteRenderer component
    public Transform characterTransform;
    public GameObject mob;
    public GameObject newMob;
    public Abilities abilities;
    public bool touchingPlayer;
    public float lookRadius = 10f;
    private int currentWaypoint = 0;    // The waypoint we are currently moving towards
    public float nextWaypointDistance = 3;     // The max distance from the AI to a waypoint for it to continue to the next waypoint

    public Transform target;    // What to chase
    public float updateRate = 2f;   // How many times each second we will update our path
    public NavMeshAgent agent;
    Seeker seeker;
    public Path path; // The calculated path

    public ForceMode fMode;

    [HideInInspector]
    public bool pathIsEnded = false;


    // Abilities
    [System.Serializable]
    public class Abilities
    {
        public Vector3 shotSpawnVector; // The vector3 storing the vector values of the shotSpawn
        public float fireRate = 0.5f; // The rate of fire for abilities
        public float nextFire = 0.0f; // Variable for the time between fires; Cooldown is added to this
        public Bite bite;

        public GameObject shot;
        public Transform shotSpawn;

        // Bite (placeholder template)
        [System.Serializable]
        public class Bite
        {
            public Rigidbody2D rb; // Rigbody for Physics                      
            public SpriteRenderer SpriteRenderer; // Variable to hold a reference to our SpriteRenderer component
            public Animator weaponAttackAnimator;
            public Animator animatorShotSpawn;
            public Transform shotSpawn; // Variable holding reference to the shotSpawn 
            public float damage = 2.0f; // Amount of damage skill does to enemies
            public float cooldown = 1.0f; // Cooldown variable/time
        }
    }


    public void OnPathComplete(Path p)
    {
        Debug.Log("Using path " + p + " Any errors: " + p.error);
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    IEnumerator UpdatePath()
    {
        if(target == null)
        {
            // TODO: Add ability to search for player later
            yield return null;
        }

        seeker.StartPath(transform.position, target.position, OnPathComplete);  // Start a new path to the target position, return the result to the OnPathComplete method

        yield return new WaitForSeconds(1f / updateRate);
        StartCoroutine(UpdatePath());
    }

    //void Start runs code at the startup
    void Start()
    {
        physics.rb = GetComponent<Rigidbody>();
        seeker = GetComponent<Seeker>();
        target = PlayerManager.instance.player.transform;
        if (target == null)
        {
            Debug.LogError("No Player (target) found");
            return;
        }
        seeker.StartPath(transform.position, target.position, OnPathComplete);  // Start a new path to the target position, return the result to the OnPathComplete method

        StartCoroutine(UpdatePath());

        agent = GetComponent<NavMeshAgent>();
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            touchingPlayer = true;
        }
        else
        {
            touchingPlayer = false;
        }
    }


    //void Update runs code every frame
    void Update()
    {
        // characterTransform.Translate(Vector3.left * Time.deltaTime, Space.World); - IE: ALWAYS GO LEFT
        GameObject playerObject = GameObject.FindWithTag("Player");
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= lookRadius)
        {
            agent.SetDestination(target.position);
        }

        // Checks to see if the variable isn't empty (we have a reference to our SpriteRenderer)
        if (characterSpriteRenderer != null)
        {
            characterTransform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        }


        // Abilities
        if (gameObject.name == "Enemy")
        {
            // Bite Ability
            if (touchingPlayer == true && Time.time > abilities.nextFire)
            {
                abilities.nextFire = Time.time + abilities.bite.cooldown;
                DestroyByMobDamage destroybymobdamage = gameObject.GetComponent<DestroyByMobDamage>();
                destroybymobdamage.abilityName = "bite";
                //abilities.bite.animatorShotSpawn = Instantiate(abilities.bite.weaponAttackAnimator, abilities.bite.shotSpawn);
                //abilities.bite.animatorShotSpawn.transform.parent = null;
                //abilities.bite.animatorShotSpawn.transform.localScale = abilities.bite.animatorShotSpawn.transform.parent.InverseTransformPoint(position: new Vector3(4.0f, 2.0f, 4.0f));
                Debug.Log("I'm biting you!");
                new WaitForSeconds(abilities.bite.cooldown);
                //weaponAttack = GetComponent<Animator>();
                //weaponAttack.SetBool("StartBool", true);
                //weaponAttack.Play("Projectile1-24 Animation");
                //GetComponent<AudioSource>().Play ();
                //weaponAttack.SetBool("StartBool", false);
            };
        }
    }

    //void FixedUpdate runs code (at the end of?) every frame
    void FixedUpdate()
    {
        if (stats.health <= 0)
        {
            Destroy(gameObject);
        }

        if (target == null)
        {
            // TODO: Add ability to search for player later
            return;
        }

        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            if (pathIsEnded)
                return;
            Debug.Log("End of path reached.");
            pathIsEnded = true;
            return;
        }
        pathIsEnded = false;

        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;    // Direction to the next waypoint
        dir *= physics.speed * Time.fixedDeltaTime;

        physics.rb.AddForce(dir, fMode);    // Move the AI

        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
        if (dist < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }

        // TODO: Always look at player
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    // This function is called just one time by Unity the moment the game loads
    private void Awake()
    {
        // get a reference to the SpriteRenderer component on this gameObject
        characterSpriteRenderer = GetComponent<SpriteRenderer>();
    }
}