﻿//Libraries/APis
using UnityEngine;
using System.Collections;

public class DestroyByDamage : MonoBehaviour
{

    //Variables
    public Rigidbody rb;
    public GameObject vfx1;
    public GameObject vfx2;
    public GameObject enemyObject;
    public GameObject weaponObject;
    public PlayerController playercontroller;
    public MobController mobcontroller;


    IEnumerator VFXDelay(float time)
    {
        //Instantiate VFX GameObject assigned to vfx1 variable on the position of the object (other) that the gameObject is colliding with
        Instantiate(vfx1, transform.position, transform.rotation);
        yield return new WaitForSeconds(time);
        Destroy(GameObject.FindWithTag("ParticleEffect"));
    }



    //When something enters collider trigger area
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Boundary")
        {
            Debug.Log("[other.gameObject]Object in Collision with this one is " + other.gameObject);
            Debug.Log("[other.gameObject.tag]Tag of object in Collision with this one is " + other.gameObject.tag);
        }

        if (other.gameObject.tag == "Player")
        {
            return;
        }

        if (other.gameObject.tag == "Enemy")
        {
            // Finds the gameObject of the 'other', the object that the object attached to this script is colliding with. In this case it is the object with an enemy tag.
            enemyObject = other.gameObject;
            // Find the Player's GameObject
            GameObject playerObject = GameObject.FindWithTag("Player");
            weaponObject = GameObject.FindWithTag("Gun");

            Destroy(gameObject);

            // Checks for MobController component stored in mobcontroller. If null, assign value to it by finding the MobController component in the enemy's GameObject (stored in enemyObject).
            if ((mobcontroller == null) && (enemyObject.GetComponent<MobController>().stats != null))
            {
                // Find MobController type component
                MobController mobcontroller = enemyObject.GetComponent<MobController>();
                // Log mobcontroller stored value
                Debug.Log("[mobcontroller] is " + mobcontroller);
                // Log enemy health
                Debug.Log("[mobcontroller.enemy.health]Enemy Health: " + mobcontroller.stats.health);

                if ((mobcontroller.stats.health != 0) && (mobcontroller != null))
                {
                    // Subtracts amount of damage done by player's ability from the mob's health
                    mobcontroller.stats.health -= weaponObject.GetComponent<Weapon>().damage;
                    Debug.Log("[mobcontroller.stats.health]Post-Attack Enemy Health: " + mobcontroller.stats.health);
                    //Destroy object
                    Destroy(gameObject);
                }
            }
            // If there is no MobController component assigned to this GameObject, output log text for debugging
            else
            {
                Debug.LogWarning("Missing MobController component. Please add one");
            }

            Destroy(GameObject.FindWithTag("ParticleEffect"), 1);
            StartCoroutine(VFXDelay(5));
        }
    }
}