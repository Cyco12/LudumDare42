﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{

    public Texture2D crosshairTexture;
    public CursorMode curMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

	// Use this for initialization
	void Start ()
    {
        Cursor.SetCursor(crosshairTexture, hotSpot, curMode);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
