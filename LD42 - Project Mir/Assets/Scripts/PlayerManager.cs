﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public GameObject player;
    /*public GameObject playerVar;
    public Transform playerSpawn;*/

    void Start()
    {
        //playerVar = Instantiate(player, playerSpawn);
        // playerVar.transform.parent = null; makes playerVar independant GameObject instead of the child of PlayerSpawn and grandchild of Game Controller
        //player.SetActive(true);
    }

    #region Singleton

    public static PlayerManager instance;

    void Awake()
    {
        instance = this;
    }

    #endregion
}
