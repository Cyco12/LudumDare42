﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSpawn : MonoBehaviour
{
    public GameObject spawnee;
    public Transform spawneeTransform;
    public bool stopSpawning = false;
    public float spawnTime;
    public float spawnDelay;
    public int maxSpawn = 3;
    public int spawnedCount = 0;

    //Variables

    //Text Variables
    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;
    public GUIText debugText01;
    public GUIText debugText02;
    public GUIText debugText03;

    //Game State Variables
    private bool gameOver;
    private bool restart;
    private int score;
    private int money;

    //Double Tap Variables
    private int tapCount = 0;
    private bool startTimer;
    private float afs = 0.0f;

    //Player Variables
    private GameObject player;
    private Vector3 playerpos = new Vector3(0, 0, 0);
    private Quaternion playerrotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);


    //Runs at start of game
    void Start()
    {
        gameOver = false;           //Game Over bool
        restart = false;            //Restart bool
                                    //Load ship number
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
        restartText.text = "";
        gameOverText.text = "";
        score = 0;                  //Score value
        money = PlayerPrefs.GetInt("Data01", 1);
    }


    public void SpawnObject()
    {
        if (spawnedCount == maxSpawn)
        {
            stopSpawning = true;
        }
        if (stopSpawning)
        {
            CancelInvoke("SpawnObject");
        }

        spawneeTransform.position = new Vector3(spawneeTransform.position.x, spawneeTransform.position.y, -1.1f);

        Instantiate(spawnee, spawneeTransform.position, spawneeTransform.rotation);
        spawnedCount++;
    }


    public void Gameover()
    {
        gameOverText.text = "Game Over!";
        money = money + score / 10;
        PlayerPrefs.SetInt("Data01", money);
        //debugText03.text = "" + money;
        PlayerPrefs.Save();
        gameOver = true;
    }
}
