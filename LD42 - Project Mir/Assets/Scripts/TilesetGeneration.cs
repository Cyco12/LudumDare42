﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesetGeneration : MonoBehaviour
{
    Vector2 worldSize = new Vector2(4, 4);
    Room[,] rooms;
    List<Vector2> takenPositions = new List<Vector2>();
    public int gridSizeX, gridSizeY, numberOfRooms = 20;
    public GameObject roomWhiteObj;
    public Enemies enemies;
    public int roomXSize = 9;
    public int roomYSize = 9;

    [System.Serializable]
    public class Objects
    {
        public int objectsPerRoom = 4;
    }

    [System.Serializable]
    public class Enemies
    {
        public int enemiesPerRoom = 4;
        public List<GameObject> enemyTypes = new List<GameObject>();
    }

    Vector2 NewPosition()
    {
        int x = 0, y = 0;
        Vector2 checkingPos = Vector2.zero;
        do
        {
            int index = Mathf.RoundToInt(Random.value * (takenPositions.Count - 1));
            x = (int)takenPositions[index].x;
            y = (int)takenPositions[index].y;
            bool UpDown = (Random.value < 0.5f);
            bool positive = (Random.value < 0.5f);
            if (UpDown)
            {
                if (positive)
                {
                    y += 1;
                }
                else
                {
                    y -= 1;
                }
            }
            else
            {
                if (positive)
                {
                    x += 1;
                }
                else
                {
                    x -= 1;
                }
            }
            checkingPos = new Vector2(x, y);
        }
        while (takenPositions.Contains(checkingPos) || x >= gridSizeX || x < -gridSizeX || y >= gridSizeY || y < -gridSizeY);
        return checkingPos;
    }

    Vector2 SelectiveNewPosition()
    {
        int index = 0, inc = 0;
        int x = 0, y = 0;
        Vector2 checkingPos = Vector2.zero;
        do
        {
            inc = 0;
            do
            {
                index = Mathf.RoundToInt(Random.value * (takenPositions.Count - 1));
                inc++;
            }
            while (NumberOfNeighbors(takenPositions[index], takenPositions) > 1 && inc < 100);
            x = (int)takenPositions[index].x;
            y = (int)takenPositions[index].y;
            bool UpDown = (Random.value < 0.5f);
            bool positive = (Random.value < 0.5f);
            if (UpDown)
            {
                if (positive)
                {
                    y += 1;
                }
                else
                {
                    y -= 1;
                }
            }
            else
            {
                if (positive)
                {
                    x += 1;
                }
                else
                {
                    x -= 1;
                }
            }
            checkingPos = new Vector2(x, y);
        }
        while (takenPositions.Contains(checkingPos) || x >= gridSizeX || x < -gridSizeX || y >= gridSizeY || y < -gridSizeY);
        if (inc >= 100)
        {
            Debug.Log("Error: Could not find position with only one neighbor");
        }
        return checkingPos;
    }

    int NumberOfNeighbors(Vector2 checkingPos, List<Vector2> usedPositions)
    {
        int ret = 0;
        if (usedPositions.Contains(checkingPos + Vector2.right))
        {
            ret++;
        }
        if (usedPositions.Contains(checkingPos + Vector2.left))
        {
            ret++;
        }
        if (usedPositions.Contains(checkingPos + Vector2.up))
        {
            ret++;
        }
        if (usedPositions.Contains(checkingPos + Vector2.down))
        {
            ret++;
        }
        return ret;
    }

    void SetRoomDoors()
    {
        for (int x = 0; x < ((gridSizeX * 2)); x++)
        {
            for (int y = 0; y < ((gridSizeY * 2)); y++)
            {
                if (rooms[x,y] == null)
                {
                    continue;
                }
                Vector2 gridPosition = new Vector2(x, y);
                // Check above
                if (y - 1 < 0)
                {
                    rooms[x, y].doorBot = false;
                }
                else
                {
                    rooms[x, y].doorBot = (rooms[x, y - 1] != null);
                }
                // Check below
                if (y + 1 >= gridSizeY * 2)
                {
                    rooms[x, y].doorTop = false;
                }
                else
                {
                    rooms[x, y].doorTop = (rooms[x, y + 1] != null);
                }
                // Check left
                if (x - 1 < 0)
                {
                    rooms[x, y].doorLeft = false;
                }
                else
                {
                    rooms[x, y].doorLeft = (rooms[x - 1, y] != null);
                }
                // Check right
                if (x + 1 >= gridSizeX * 2)
                {
                    rooms[x, y].doorRight = false;
                }
                else
                {
                    rooms[x, y].doorRight = (rooms[x + 1, y] != null);
                }
            }
        }
    }

    void CreateRooms()
    {
        // Setup
        rooms = new Room[gridSizeX * 2, gridSizeY * 2];
        rooms[gridSizeX, gridSizeY] = new Room(Vector2.zero, 1);
        takenPositions.Insert(0, Vector2.zero);
        Vector2 checkPos = Vector2.zero;

        // Magic numbers
        float randomCompare = 0.2f, randomCompareStart = 0.2f, randomCompareEnd = 0.01f;
        // Add rooms
        for (int i = 0; i < numberOfRooms -1; i++)
        {
            float randomPerc = ((float)i) / (((float)numberOfRooms - 1));
            randomCompare = Mathf.Lerp(randomCompareStart, randomCompareEnd, randomPerc);
            // Grab new position
            checkPos = NewPosition();
            // Test new position
            if (NumberOfNeighbors(checkPos, takenPositions) > 1 && Random.value > randomCompare)
            {
                int iterations = 0;
                do
                {
                    checkPos = SelectiveNewPosition();
                    iterations++;
                }
                while (NumberOfNeighbors(checkPos, takenPositions) > 1 && iterations < 100);
                if (iterations >= 50)
                    Debug.Log("Error: Could not create with fewer neighbors than: " + NumberOfNeighbors(checkPos, takenPositions));
            }
            // Finalize position
            rooms[(int)checkPos.x + gridSizeX, (int)checkPos.y + gridSizeY] = new Room(checkPos, 0);
            takenPositions.Insert(0, checkPos);
        }
    }

    void DrawMap()
    {
        foreach (Room room in rooms)
        {
            if (room == null)
            {
                continue;
            }
            Vector2 drawPos = room.gridPos;
            drawPos.x *= roomXSize;
            drawPos.y *= roomYSize;
            TileSpriteSelector mapper = GetComponent<TileSpriteSelector>(); // = Object.Instantiate(GetComponent<TileSpriteSelector>().PickTile(), drawPos, Quaternion.identity).GetComponent<TileSpriteSelector>();
            mapper.type = room.type;
            mapper.top = room.doorTop;
            mapper.bottom = room.doorBot;
            mapper.right = room.doorRight;
            mapper.left = room.doorLeft;
            Object.Instantiate(mapper.PickTile(), drawPos, Quaternion.identity).GetComponent<TileSpriteSelector>();


            /*Vector2 roomCenterPos = room.gridPos;
            float coordXRangeMath = room.gridPos.x - roomXSize / 2f;
            float coordYRangeMath = (room.gridPos.y - roomYSize / 2f) - 1;

            for (int i = 0; i <= enemies.enemiesPerRoom; i++)
            {
                GameObject selectEnemy;
                if (enemies.enemyTypes.Count > 1)
                {
                    selectEnemy = enemies.enemyTypes[Random.Range(0, enemies.enemyTypes.Count)];
                    float randomCoordInRangeX = Random.Range(coordXRangeMath, roomXSize);
                    float randomCoordInRangeY = Random.Range(coordYRangeMath, roomYSize);
                    selectEnemy.transform.position = new Vector3(randomCoordInRangeX, randomCoordInRangeY, -1.1f);
                    Instantiate(selectEnemy, selectEnemy.transform);
                }
                if (enemies.enemyTypes.Count == 1)
                {
                    selectEnemy = enemies.enemyTypes[0];
                    float randomCoordInRangeX = Random.Range(coordXRangeMath, roomXSize);
                    float randomCoordInRangeY = Random.Range(coordYRangeMath, roomYSize);
                    selectEnemy.transform.position = new Vector3(randomCoordInRangeX, randomCoordInRangeY, -1.1f);
                    Instantiate(selectEnemy, selectEnemy.transform);
                }
                return;
            }*/
        }
    }

    void TilePopulate()
    {
        // Use array Rooms to find coordinate positions to the center of rooms, use size of room variables to find boundaries, create ranges using those boundaries
        foreach (Room room in rooms)
        {
            if (room == null)
            {
                continue;
            }
            Vector2 roomCenterPos = room.gridPos;
            float coordXRangeMath = room.gridPos.x - roomXSize / 2f;
            float coordYRangeMath = (room.gridPos.y - roomYSize / 2f) - 1;

            for (int i = 0; i <= enemies.enemiesPerRoom; i++)
            {
                GameObject selectEnemy = enemies.enemyTypes[Random.Range(0, enemies.enemyTypes.Count + 1)];
                float randomCoordInRangeX = Random.Range(coordXRangeMath, roomXSize);
                float randomCoordInRangeY = Random.Range(coordYRangeMath, roomYSize);
                selectEnemy.transform.position = new Vector3(randomCoordInRangeX, randomCoordInRangeY, -1.1f);

                Instantiate(selectEnemy, selectEnemy.transform);

                Debug.LogWarning("Spawned " + selectEnemy + " at " + selectEnemy.transform);
            }
            //Vector2 roomXBoundary = new Vector2(room.gridPos.x - roomXSize/2, room.gridPos.y - roomYSize / 2);

        }
    }

    void Start()
    {
        if (numberOfRooms >= (worldSize.x * 2) * (worldSize.y * 2))
        {
            numberOfRooms = Mathf.RoundToInt((worldSize.x * 2) * (worldSize.y * 2));
        }
        gridSizeX = Mathf.RoundToInt(worldSize.x);
        gridSizeY = Mathf.RoundToInt(worldSize.y);
        CreateRooms();
        SetRoomDoors();
        DrawMap();
        TilePopulate();
    }
}
