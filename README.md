# LudumDare42
My group's entry for the Ludum Dare #42, ZeroBytes!

The controls are:
WASD/Arrow Keys to move
E to interact
Mouse1 to shoot
Escape to pull up menus while in-game

#
## The theme is: "Running out of space"
#
#
## Using:
- A * Pathfinding Project; https://arongranberg.com/astar/download
#
#
### Notes
- Arcade styled game
#
#
### Ideas:
~- Tower that you have to escape where you rooms and get out of a constantly shrinking area.~

- You're running out of space on your computer, you have to 'delete' programs by defeating mobs (desktop icons)
  ~- You have to work through different environments on your computer to defeat programs to gain back space; exploring/adventure based dungeon game~
  - You have multiple levels/rooms/files(folders); enter the gungeon style floor based game
  ~- You have to defeat bosses (applications) in each room; bossfight style game~
  - Spawn in base/arsenal area to purchase powerups/gear before hand.
  - Timer mechanic where you lose space the longer you take, but you can kill things/clear rooms to gain back space.
  - Enemies only spawn when you're in the room.
  - Menu is the Desktop background/screen
  - Start in 'protected' area (CPU room) thats like an arsenal or a base where you can loadout and prepare for the levels
  - Enter 'infected' folders or floors where you have a procedurally generated floor with a variety of tiles/rooms spawned randomly (this adds replayability)
  - You have to clear the whole floor before going back to the CPU room. This allows you to upgrade your gear and continue to work towards your goal of freeing up space in the system.
  - Your overall goal/win condition is freeing up space in the system by killing enemies/clearing folders.
  - Retro old windows 1998/Synthwave/old CRT monitor art style
